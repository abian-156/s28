//Create the Schema, model of each user and make sure to expose the file afterwards
const mongoose = require('mongoose');

const userBlueprint = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'first Name is Required']
	},
	lastName: {
		type: String,
		required: [true, 'last Name is Required']
	},
	email: {
		type: String,
		required: [true, 'Email is Required']
	},
	password: {
		type: String,
		required: [true, 'Password is Required']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	//The "tasks" field will be an 'array of objects', this will contain a list of tasks assigned for this user.
	tasks: [
		{
			tasksId: {
				type: String,
				required: [true, 'Task Id is Required']
			},
			assignedOn: {
				type: Date,
				default: new Date()
			}
		}
	] 
});

//model for users
module.exports = mongoose.model("User", userBlueprint);