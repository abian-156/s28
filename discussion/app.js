//[SECTION] Dependencies and Module
	const express = require('express');
	const mongoose = require('mongoose');
	const dotenv = require('dotenv');
	const taskRoutes = require('./routes/tasks');
	const userRoutes = require('./routes/users');

//[SECTION] Environment VAriables
	dotenv.config();
	let secret = process.env.CONNECTION_STRING;
	

//[SECTION] Server Setup
	const server = express();
	server.use(express.json()); //middleware
	const port = process.env.PORT;

//[SECTION]	Server Routes
	server.use('/tasks', taskRoutes);
	server.use('/users',userRoutes);

//[SECTION]	Database Connection
	mongoose.connect(secret);
	
	let db = mongoose.connection;
	db.once('open', () => console.log('Now connected to MongooDB Atlas'));

//[SECTION] Server Response
	server.listen(port, () => console.log(`Server is running  on ${port}`));
	server.get('/', (req, res) => {
		res.send('Welcome to App!');
	});