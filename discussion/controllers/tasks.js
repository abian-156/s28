//[SECTION] Dependencies and Modules
const Task = require('../models/Task');


//[SECTION] Functionalities

//Create New Task
	module.exports.createTask =(clientInput) => {
		let taskName = clientInput.name;
		let newTask = new Task ({
			name: taskName
		});

		return newTask.save().then((task, error) => {
			if (error) {
				return 'Saving New Task Failed'
			} else {
				return 'A New Task Has Been Created!'
			}
		});

	};

//Retrieve All Tasks
	module.exports.getAllTasks = () => {
		return Task.find({}).then(searchResult => {
			return searchResult;
		});
	};

//Retrieve a single user
	module.exports.getTask = (data) => {
		return Task.findById(data).then(result => { 
			return result;
		})
	};


//Delete a user
	module.exports.deleteTask = (taskId) => {
		return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
			if (removedTask) {
				return 'Task Deleted Successfully';
			} else {
				return 'No Task Were Removed!';
			}
		});
	};

//Update task status
	module.exports.updateTask = (taskId, newContent) => {
		let status = newContent.status;
		return Task.findById(taskId).then((foundTask, error) => {
			if (foundTask) {
				foundTask.status = status;
				return foundTask.save().then((updatedTask, saveErr) => {
					if (saveErr) {
						return false;
					} else {
						return updatedTask;
					}
				});
			} else {
				return ' Task Was Found';
			}
		});
	};

	
