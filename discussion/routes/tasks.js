//[SECTION] Dependecies and Modules
	const express = require('express');
	const controller = require('../controllers/tasks');

//[SECTION] Routing Component
	const route = express.Router();

//[SECTION] Task Routes
//Create task
	route.post('/', (req, res) => {
		//excute the creatTask() from controller.
		let taskInfo = req.body;
		controller.createTask(taskInfo).then(result =>
			res.send(result)
		);
	});

//Retrieve all Task
	route.get('/', (req, res) => {
		controller.getAllTasks().then(result => {
			res.send(result);
		});
	});

//Retrieve task
	route.get('/:id', (req, res) => {
		let taskId = req.params.id;
		controller.getTask(taskId).then(result => {
			res.send(result);
		});
	});

//delete Task
	route.delete('/:id', (req, res) => {
		let taskId = req.params.id
		controller.deleteTask(taskId).then(result => {
			res.send(result);
		});
	});


//Update Task
	route.put('/:id', (req, res) => {
		let id = req.params.id;
		let task = req.body;
		let katawan = req.body
		controller.updateTask(id, katawan).then(outcome => {
			res.send(outcome);
		});
		
	});

//[SEction] Expose Route System
	module.exports = route;
